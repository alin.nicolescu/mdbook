IN = ./markdown/*.md
OUT = ./output
DATE = `date +"%d-%m-%Y-%T"`
FILE_NAME = Book-$(DATE).pdf
# FILE_NAME = Book-$(shell date +"%d-%m-%Y-%T").pdf
META = ./metainfo/metadata.yaml
LATEX_META = ./metainfo/latex.yaml
FILTER = --filter pandoc-latex-environment
FLAGS = --metadata-file=$(META) \
		--metadata-file=$(LATEX_META) \
		--from markdown \
		--pdf-engine=xelatex \
		--top-level-division=chapter \
		$(FILTER)

pdf: $(IN) $(META) $(LATEX_META)
	pandoc $(IN) --output=$(OUT)/$(FILE_NAME) $(FLAGS)

.PHONY: clean

clean:
	rm -fv $(OUT)/*.pdf
