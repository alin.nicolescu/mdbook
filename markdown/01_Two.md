# Hyperlinks and images

Here is a link to the [Pandoc User's Guide](https://pandoc.org/MANUAL.html).

Here is a picture of **Tux**, the official **Linux** mascot created by
Larry Ewing (<lewing@isc.tamu.edu>) in 1996:

![Tux](resources/Tux.png){height=25%}

