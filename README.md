# Simple Markdown book template

Generates a PDF book based on the standard book `LaTeX` class.

- `B5` paper
- title page
- table of contents
- chapter title centered in header
- page number centered in footer
- 0.4pt width rule for both header and footer

## Project structure

```
mdbook/
├── markdown
│   ├── 00_One.md
│   ├── 01_Two.md
│   └── 02_Three.md
├── metainfo
│   ├── latex.yaml
│   └── metadata.yaml
├── output
│   └── Book.pdf
├── resources
│   └── Tux.png
├── LICENSE
├── Makefile
└── README.md
```

The markdown source chapters must be `md` files placed in the `markdown`
directory.

Additional files (images etc.) must be placed in `resources` directory.

Additional `LaTeX` packages must be added in `latex.yaml` file.

Title, author, page size etc. are placed in `metadata.yaml` file.

The generated PDF file is in the `output` directory.

## Usage

Run `make` from the project's directory.

## Requirements

- [`LaTeX`](https://www.latex-project.org/)
- [`Pandoc`](https://pandoc.org/)
- [`GNU Make`](https://www.gnu.org/software/make/)
- For `awesomeboxes` install `pandoc-latex-environment` filter:

    ```bash
    sudo pip install pandoc-latex-environment
    ```
