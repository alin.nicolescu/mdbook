# Awesome boxes


Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam lobortis
facilisis sem. Nullam nec mi et neque pharetra sollicitudin. Praesent imperdiet
mi nec ante.

::: note
Donec ullamcorper, felis non sodales commodo, lectus velit
ultrices augue, a dignissim nibh lectus placerat pede.
:::

::: important
Vivamus nunc nunc, molestie ut, ultricies vel, semper in, velit.
:::

::: warning
Ut porttitor. Praesent in sapien.
:::

::: caution
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Duis fringilla
tristique neque.
:::

::: tip
Sed interdum libero ut metus.
:::

Pellentesque placerat. Nam rutrum augue a leo. Morbi sed elit sit amet ante
lobortis sollicitudin. Praesent blandit blandit mauris. Praesent lectus tellus,
aliquet aliquam, luctus a, egestas a, turpis.
