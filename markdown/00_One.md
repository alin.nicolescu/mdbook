# Markup for text

## Paragraphs

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam lobortis
facilisis sem. Nullam nec mi et neque pharetra sollicitudin. Praesent imperdiet
mi nec ante. Donec ullamcorper, felis non sodales commodo, lectus velit
ultrices augue, a dignissim nibh lectus placerat pede. Vivamus nunc nunc,
molestie ut, ultricies vel, semper in, velit. Ut porttitor. Praesent in sapien.
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Duis fringilla
tristique neque. Sed interdum libero ut metus. Pellentesque placerat. Nam
rutrum augue a leo. Morbi sed elit sit amet ante lobortis sollicitudin.
Praesent blandit blandit mauris. Praesent lectus tellus, aliquet aliquam,
luctus a, egestas a, turpis.

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam lobortis
facilisis sem. Nullam nec mi et neque pharetra sollicitudin. Praesent imperdiet
mi nec ante. Donec ullamcorper, felis non sodales commodo, lectus velit
ultrices augue, a dignissim nibh lectus placerat pede. Vivamus nunc nunc,
molestie ut, ultricies vel, semper in, velit. Ut porttitor. Praesent in sapien.
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Duis fringilla
tristique neque. Sed interdum libero ut metus. Pellentesque placerat. Nam
rutrum augue a leo. Morbi sed elit sit amet ante lobortis sollicitudin.
Praesent blandit blandit mauris. Praesent lectus tellus, aliquet aliquam,
luctus a, egestas a, turpis.

## Blockquote

> Sed interdum libero ut metus. Pellentesque placerat. Nam rutrum augue a leo.
> Morbi sed elit sit amet ante lobortis sollicitudin.
> Praesent blandit blandit mauris. Praesent lectus tellus, aliquet aliquam,
> luctus a, egestas a, turpis.

## Some list

- Item
- Another item
- Last item

## An enumeration

1. **This text is bold**
1. *This text is italic*
1. ~~This text is striked~~

## Source code

```cpp
#include<iostream>
using namespace std;

int main()
{
    cout<<"Hello world\n";
    return 0;
}
```

## A simple table

| First column | Second column | Last column  |
| :----------: | :------------ | -----------: |
| Center       | Left          | Right        |

## \LaTeX{} equations

For $ax^{2}+bx+c=0$ we calculate $\Delta=b^{2}-4ac$ and then, if $\Delta>0$
then the two solutions are:

$$\frac{-b \pm \sqrt{\Delta}}{2a}$$
